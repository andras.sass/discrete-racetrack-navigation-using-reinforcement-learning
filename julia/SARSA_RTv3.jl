﻿###                                                             ###
###                                                             ###
###                  SARSA for the RACETRACK PROBLEM            ###
###                                                             ###
###                                                             ###



#   1.) compile this file via:
#           julia> include("SARSA_for_plot.jl")
#
#   2.) run the "createenvironment" function via:
#           julia> createenvironment(i,v)
#
#       i...racetrack number:
#               * i=1 small racetrack
#               * i=2 medium racetrack
#               * i=3 large racetrack
#       v...velocity options:
#               * v=1 low velocities are possible
#               * v=2 medium velocities are possible
#               * v=3 high velocities are possible
#
#
#   3.) run the "train" function via:
#           julia> train(n,gammay)
#
#       n      ... number of training episodes:
#                   * The bigger the environment the bigger the n should be.
#                   * n has a default value depending on the size of the environment.
#       gammay ... farsightedness of the Agent:
#                   * gamma should be between 0 and 1.
#                   * The bigger gamma, the more farsighted the agent will be
#                     when learning.
#
#
#   4.) plot an example episode using your calculated policy via:
#           julia> plotepisode()
#



# CODE:




#           FIRST:
#           We define the "createenvironment" function

function createenvironment(i = 1 , v::Int64 = 1)



    # We generate the selected RACETRACK (i=1,2,3), which is a 3-dimensional
    # kxk matrix:

    #   Each entry of the matrix corresponds to one field on the racetrack.
    #   The possible entries of the matrix are -1,0,1,2 :
    #      -1...not part of the track
    #       1...part of the finish line
    #       2...part of the starting line
    #       0...part of the track, but neither finish line nor starting line

    # ATTENTION:    The Racetrack should be visualized as the matrix turned
    #               by 90° counterclockwise.
    # BECAUSE:      A point (a,b) in the standard (x,y)-coordinate system we are
    #               used to goes a to the right and b up.
    #               But when accessing a matrix at the point (a,b) we go a down
    #               and b to the left.


    if     i == 1
        global racetrack = [-1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  1  1  1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1]

    elseif i == 2
        global racetrack = [-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0  0  0  0  1 -1;
                            -1 -1 -1 -1 -1  0  0  0  0  0  0  1 -1;
                            -1 -1 -1 -1 -1  0  0  0  0  0  0  1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1]

                        elseif i == 3
                            global	racetrack	=	[	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	2	0	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	2	0	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	2	0	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	0	0	0	1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	0	0	0	1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	0	0	0	0	0	0	1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	;
                                                    	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	-1	]

                        elseif typeof(i) == Array{Int64,2}
                            global racetrack = i
                        end



    # We set the maximum x and y coordinates which our agent can reach and the
    # maximum velocities:

    global nx = length(racetrack[:,1])
    global ny = length(racetrack[1,:])

    global rewards = zeros(nx,ny)
    for i = 1:nx
        for j = 1:ny
            if racetrack[i,j] == -1
                rewards[i,j] = 0
            elseif racetrack[i,j] == 0 || racetrack[i,j] == 2
                rewards[i,j] = -1
            elseif racetrack[i,j] == 1
                rewards[i,j] = 10000
            end
        end
    end

    global maxv = 1 + v
        # This is equivalent to:
        #        if v == 1
        #            maxv = 2 ;
        #        elseif v ==2
        #            maxv = 3 ;
        #        elseif v == 3
        #            maxv = 4 ;
        #        end



    # We determine the startfields:

    global startfields = []
    for x = 1:nx
        for y = 1:ny
            if racetrack[x,y] == 2
                startfields = [ startfields ; [x y] ]
            end
        end
    end

    global numstart  = size(startfields)[1]

    # We generate the STATESPACE, which is a matrix:
    #       [x,y,vv,hv;
    #        x,y,vv,hv;
    #        x,y,vv,hv;
    #            :
    #            :
    #        x,y,vv,hv]
    #
    # Every row corrsponds to one state, which the agent can take:
    #        [x,y,hv,vv] ... One possible state:
    #                 x  ... horizontal position (only positive)
    #                 y  ... vertical position  (only positive)
    #                 hv ... horizontal velocity:
    #                           * hv = -m ... Agent goes m fields to the left
    #                           * hv = m ... Agent goes m fields to the right
    #                 vv ... vertical velocity:
    #                           * vv = -m ... Agent goes m fields down
    #                           * vv = m ... Agent goes m fields up

    global ns = 0
    for x = 1:nx
        for y = 1:ny
            if racetrack[x,y] != -1
                ns += 4*maxv^2+4*maxv +1
            end
        end
    end


    global S = zeros(Int64, ns, 4)
    indx = 1

    for x = 1:nx
        for y = 1:ny
            if racetrack[x,y] != -1
                for hv = -maxv:maxv
                    for vv = -maxv:maxv
                            S[indx,:] = [x y hv vv]
                            indx += 1
                    end
                end
            end
        end
    end



    # !!!ATTENTION!!! Part of the next step is already part of the MC - learning
    # algorithm! We do it here because like this we save runtime when training
    # and we can call consequtive "train" functions and do not destroy our
    # already learned Q and C values.

    # Now we implement the Q(s,a) - Values, which we eventually want to calculate
    # to derive the optimal policy. We save the Q - Values in an array as follows:
    #
    #       q = [xpos ypos xvel yvel changexvel changeyvel Q(s,a)
    #            xpos ypos xvel yvel changexvel changeyvel Q(s,a)
    #                :
    #                :
    #            xpos ypos xvel yvel changexvel changeyvel Q(s,a)]  ,
    #
    # so the first 4 entries in each row make up the state s, the 2 following
    # define the action a and at the 7th position in each row we save the actual
    # Q - Value for the state-action pair (s,a).


    global num_st_act = 0
    for k = 1:ns
        num_st_act += size(actionspace(S[[k],:]))[1]
    end
    indx = 1

    global q = zeros(num_st_act, 7)
    for k in 1:ns
        s  = S[[k],:]
        Actionspace = actionspace(s)
        na = size(Actionspace)[1]
        for j in 1:na
            a = Actionspace[[j],:]
            q[indx,:] = [s a [rand()]]
            indx += 1
        end
    end


    # Next we implement our policy "pol" as a matrix, with the form:
    #
    #           pol = [xpos ypos xvel yvel changexvel changeyvel
    #                  xpos ypos xvel yvel changexvel changeyvel
    #                                   :
    #                                   :
    #                  xpos ypos xvel yvel changexvel changeyvel]  ,
    #
    # where the first 4 entries of each row represent a state s and the last 2
    # entries represent the action a which (according to the policy pol) should
    # be taken.


    global pol = zeros(Int8, ns, 6)
    for k in 1:ns
        pol[k,:] = [S[[k],:] [0 0]]
    end


    # !!!ATTENTION!!! This next step is already part of the MC-learning algorithm. We
    # do it here, because like this we save runtime and we can call the "train"
    # function repeatedly, without destroying our policy pol.

    # We initialize our policy matrix pol such that
    #
    #           pol = [s argmaxQ(s,a)
    #                  s argmaxQ(s,a)
    #                       :
    #                       :
    #                  s argmaxQ(s,a)]  .
    #
    # The Q-Values have already been initialized randomly, when the environment
    # was created.


    for k = 1:ns
        s           = S[[k],:]
        Actionspace = actionspace(s)
	      temp        = callQ(s,Actionspace[[1],:])
        max         = temp[1]
        indx        = temp[2]
        argmax      = Actionspace[[1],:]

        for j = 2:size(Actionspace)[1]
            a   = Actionspace[[j],:]
	        qsa = q[indx+j-1,7]
            if max < qsa

                max    = qsa
                argmax = a
            end

        end
	    pol[k,5:6] = argmax
    end


    # finally we define a counting variable tot_ep, which counts how many training
    # episodes have been done totally.

    global tot_ep = 0

    # Also we define a bollean variable, which tells us whether the training algorithm
    # has converged yet or not.

    global converged = false

    # Also a variable which represents the reward, which is received when Following
    # the calculated policy.

    global previousR = -Inf


    println()
    println("A new environment + agent has been created. Ready for train(n,gammay)")
    println()

end


# We create the function actionspace(s), which returns the actionspace
#
#           A(s) = [changehv,changevv
#                   changehv,changevv
#                           :
#                           :
#                   changehv,changevv]
#
# of the agent, when he is in state s = [x,y,hv,vv], where each row
# [changehv,changevv] corresponds to one posible action the agent can take
# when he is in state s.


function actionspace(s)

    local A                                     =   []
    local resultingvelocity::Array{Float64,2}   =   [0 0]
    local currentvelocity::Array{Float64,2}     =   s[[3 4]]

    for k = -1:1
        for j = -1:1

            resultingvelocity   =   currentvelocity .+ [k j]
            if  (   abs(resultingvelocity[1])  <=   maxv  &&
                    abs(resultingvelocity[2])  <=   maxv  &&
                    resultingvelocity          !=   [0 0]   )

                A = [A ; [k j]]
            end

        end
    end
    return A

end


# We want to define a function which gives back the agents next state, when
# chooses an action a in state s. Before we can do that we need a function
# whih tells us which fields would be visited by the agent, if he goes
# along his decided path. We need the fieldsvisited function because we have
# to check, whether the agent will be still on the track and if he will cross
# the finish line or not.

# Next we generate the function 'fieldsvisited(s,a)' , which receives a
# a state s = [xpos,ypos,xvel,yvel] and an action a = [changehv, changevv]
# and gives back an array of all the fields that are visited by the agent if
# he is in state s, takes action a and takes a step. So we get:
#
#           fieldsvisited(s,a) = [x y
#                                 x y
#                                 x y
#                                  :
#                                  :
#                                 x y]  ,
#
# where [x y] is the x and y coordinate of a field the agent visited in this
# time step.


function fieldsvisited(s,a)

    local position       =   s[[1 2]]
    local velocity       =   s[[3 4]] .+ a
    local F              =   []
    local currentfield   =   position
    local endfield       =   position .+ velocity


    if sign(velocity[1]) == 0
        for k = 1:abs(velocity[2])
            F = [F ; [position[1] position[2]+k*sign(velocity[2])]]
        end
    elseif sign(velocity[2]) == 0
        for k = 1:abs(velocity[1])
            F = [F ; [position[1]+k*sign(velocity[1]) position[2]]]
        end
    elseif sign(velocity[1]) == sign(velocity[2])
        if sign(velocity[1]) == 1

            while currentfield != endfield
                s1 =  ((currentfield[1]+1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2)
                d1 = (currentfield[1]+1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2

                s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]+1-position[2])) / (velocity[1]^2+velocity[2]^2)
                d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]+1-(position[2]+s2*velocity[2]))^2

                if d1 < d2
                    currentfield = currentfield .+ [1 0]
                    F            = [F;currentfield]
                elseif d1 > d2
                    currentfield = currentfield .+ [0 1]
                    F            = [F;currentfield]
                else
                    currentfield = currentfield .+ [1 0]
                    F            = [F ; currentfield]
                    currentfield = currentfield .+ [-1 1]
                    F            = [F ; currentfield]
                    currentfield = currentfield .+ [1 0]
                    F            = [F ; currentfield]
                end
            end
        elseif sign(velocity[1]) == -1

            while currentfield != endfield
                s1 =  ((currentfield[1]-1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2)
                d1 = (currentfield[1]-1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2

                s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]-1-position[2])) / (velocity[1]^2+velocity[2]^2)
                d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]-1-(position[2]+s2*velocity[2]))^2

                if d1 < d2
                    currentfield = currentfield .+ [-1 0]
                    F            = [F;currentfield]
                elseif d1 > d2
                    currentfield = currentfield .+ [0 -1]
                    F            = [F;currentfield]
                else
                    currentfield = currentfield .+ [-1 0]
                    F            = [F;currentfield]
                    currentfield = currentfield .+ [1 -1]
                    F            = [F;currentfield]
                    currentfield = currentfield .+ [-1 0]
                    F            = [F ; currentfield]
                end
            end
        end
    elseif sign(velocity[1]) != sign(velocity[2])
        if sign(velocity[1]) == 1

            while currentfield != endfield
                s1 =  ((currentfield[1]+1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2)
                d1 = (currentfield[1]+1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2

                s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]-1-position[2])) / (velocity[1]^2+velocity[2]^2)
                d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]-1-(position[2]+s2*velocity[2]))^2

                if d1 < d2
                    currentfield = currentfield .+ [1 0]
                    F            = [F;currentfield]
                elseif d1 > d2
                    currentfield = currentfield .+ [0 -1]
                    F            = [F;currentfield]
                else
                    currentfield = currentfield .+ [1 0]
                    F            = [F;currentfield]
                    currentfield = currentfield .+ [-1 -1]
                    F            = [F;currentfield]
                    currentfield = currentfield .+ [1 0]
                    F            = [F ; currentfield]
                end
            end

        elseif sign(velocity[1]) == -1

            while currentfield != endfield
                s1 =  ((currentfield[1]-1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2)
                d1 = (currentfield[1]-1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2

                s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]+1-position[2])) / (velocity[1]^2+velocity[2]^2)
                d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]+1-(position[2]+s2*velocity[2]))^2

                if d1 < d2
                    currentfield = currentfield .+ [-1 0]
                    F            = [F;currentfield]
                elseif d1 > d2
                    currentfield = currentfield .+ [0 1]
                    F            = [F;currentfield]
                else
                    currentfield = currentfield .+ [-1 0]
                    F            = [F;currentfield]
                    currentfield = currentfield .+ [1 1]
                    F            = [F;currentfield]
                    currentfield = currentfield .+ [-1 0]
                    F            = [F ; currentfield]
                end
            end
        end
    end
return(F)
end


# So now we can construct the nextstate(s,a) function, which again receives
# the agents current state s = [xpos, ypos, xvel, yvel] and the action
# a = [changexv, changeyv] , which the agent is taking:
#
#           nextstate(s,a) = [newxpos,newypos,newxvel,newyvel,bool] ,
#
# where:     newxpos ... Agents new x - Position
#            newypos ... Agents new y - Position
#            newxvel ... Agents new x - Velocity
#            newyvel ... Agents new y - Velocity
#            bool    ... Finished or not?


function nextstate(s,a)

    local F             = fieldsvisited(s,a)
    local newvel        = s[[3 4]] .+ a
    local newpos        = []
    local fs            = []
    local x             = 0
    local y             = 0
    local crossedfinish = [false,0]
    local crossedborder = [false,0]
    local finished      = false


    for k = 1:size(F)[1]
      x = F[k,1]
      y = F[k,2]
      if   racetrack[x,y] == 1 && crossedfinish[1] == false
        crossedfinish = [true, k]
      elseif racetrack[x,y] == -1 && crossedborder[1] == false
        crossedborder = [true, k]
      end
    end

    if ( crossedfinish[1] == true &&
         crossedborder[1] == true &&
         crossedfinish[2]  < crossedborder[2] )

        newpos   = F[ [crossedfinish[2]] , : ]
        finished = true

    elseif ( crossedfinish[1] == true &&
             crossedborder[1] == false   )

        newpos   = F[ [crossedfinish[2]] , : ]
        finished = true

    elseif crossedborder[1] == true

        newpos   = startfields[ [rand(1:size(startfields)[1])] , : ]
        newvel   = [0 0]
    else

        newpos = s[[1 2]] .+ newvel

    end

    fs = [newpos newvel finished]
    return (fs)
end


# Later, when we are using the learning algorithm, we don't want to work with
# messy matrix-indices, so we also define 2 functions:
#
#   callQ(s,a)    ... returns Q - Value for the state-action pair (s,a)
#   writeQ(s,a,v) ... saves the value v as Q(s,a)


function callQ(s, a)
    k = find(all(q[:,1:6] .== [s a],2))[1]
    return((q[k,7][1],k[1]))
end
function writeQ(s,a,v)
    k = find(all(q[:,1:6] .== [s a],2))[1]
    q[k,7] = v
end


# Again we will define functions for calling and writing the values of pol.


function writePol(s,a)
    k = find(all(pol[:,1:4] .== s ,2))[1]
    pol[k,5]    =   a[1]
    pol[k,6]    =   a[2]
end
function callPol(s)
    k = find(all(pol[:,1:4] .== s ,2))[1]
    return(a = [ pol[k,5][1]  pol[k,6][1] ])
end


# Now we want define a function createepisode(p), which for a given
# policy p = p(s) returns a matrix containing an episode of the agent in the
# following way:
#
#           createepisode(p) = [ s0  a0  r1
#                                s1  a1  r2
#                                s2  a2  r3
#                                     :
#                                     :
#                                sT [0 0] 0]  ,
#
# where:    si ... state in timestep i
#                   * s0 is the initial state
#                   * sT is the terminal state
#           ai ... action in timestep i
#                   * aT is always [0 0]
#           ri ... reward in timestep i
#                   * there is no r0
#                   * the last reward is always 0

function createepisode(p::Function , stst = [] , soft = true)

    local a             = []
    local r             = []
    if stst == []
        local s         = [ startfields[[rand(1:size(startfields)[1])],:] [0 0] ]
    else
        s = [ stst [0 0] ]
    end
    local fs            = []
    local k             = 1
    local counter       = 1
    visited_startfields = [s[1,:]]

    while true
        a  =  [ a ; p(s[[k],:]) ]
        k  = k+1

        fs =  nextstate(s[[k-1],:],a[[k-1],:])
        r  =  [ r ; rewards[fs[1] , fs[2]] ]
        s  =  [ s ; fs[[1 2 3 4]] ]
        if fs[5] == true
            break
        elseif soft == false
            if fs[[3 4]] == [0 0] && counter >= numstart
                println("Following the policy, the agent didnt manage to get to the finish line. He started at: (",s[1,1],", ",s[1,2],")" )
                r[end] = -1
                break
            end
            if fs[[3 4]] == [0 0] && (in(vec(fs[[1 2 3 4]]),visited_startfields)==false)

                visited_startfields = [ visited_startfields [ vec(fs[[1 2 3 4]]) ] ]
                counter = counter+1

            end
        end
    end
    return([s  [a ; [0 0]]  [r ; 0]])
end






#           SECOND:
#           We define the "train" function

function train(n::Int64 = 0 , gammay::Float64 = 0.9)


    # We initialize epsi and alpha, which will gradually decrease after every
    # training episode, following the formulas:
    #       epsi  = 1/sqrt(sqrt(t)) ,
    #       alpha = 1/sqrt(sqrt(t)) ,
    # where tot_ep refers to the total number of training episodes done until
    # then.

    if tot_ep == 0
        global epsi  = 1
        global alpha = 1
    end

    # Parameters for the tradeof between exporation and expoitation
    sure  = 50
    half  = 75
    k     = log(1/9)/(sure-half)
    d     = -half * log(1/9)/(sure-half)
    # End


    # Until now, all we did, was prepare all the tools, to be able to implement
    # the actual algorithm. Thats what we are doing now:
    #(Q is already initialized.)


    for l = 1:n

        local terminal::Bool = false
        local probab         = rand(1:numstart)
        local s              = [startfields[[probab],:]  [0 0]]
        local a              = p(s)
        local counter        = 0

        while terminal == false
            next_s     = nextstate(s,a)[[1],1:4]
            r          = rewards[next_s[1], next_s[2]]
            next_a     = p(next_s)
            if counter == 0
                temp   = callQ(s,a)
            end
            Q_s_a      = temp[1]
            Indx_s_a   = temp[2]
            temp       = callQ(next_s,next_a)
            Q_ns_na    = temp[1]
            Indx_ns_na = temp[2]

            q[Indx_s_a,7] = Q_s_a + alpha * ( r + gammay * Q_ns_na - Q_s_a )

            # Update the policy to the new Q(s,a) - value:
            Actionspace = actionspace(s)
            tempr       = callQ(s,Actionspace[[1],:])
            max         = tempr[1]
            indx        = tempr[2]
            argmax      = Actionspace[[1],:]

            for j = 2:size(Actionspace)[1]
                test_a   = Actionspace[[j],:]
                qsa      = q[indx+j-1,7]

                if max < qsa
                    max    = qsa
                    argmax = test_a
                end
            end
            writePol(s,argmax)
            # Done

            s      = next_s
            a      = next_a

            if racetrack[s[1],s[2]] == 1
                terminal = true
            end

        counter += 1
        end

        println("Episodes done: ",l ," / ",n )
        global tot_ep = tot_ep + 1

        epsi  = 1/( 1+exp(k*tot_ep + d) )
        alpha = epsi

    end

    #=
    println()
    println("TRAINING DONE! NUMBER OF TOTAL TRAINING-EPISODES: ",tot_ep)
    println()

    println("Now the resulting policy is being evaluated from the calculated Q-values...")
    println()

    for k = 1:ns
        s      = S[[k],:]
	    temp   = callQ(s,actionspace(s)[[1],:])
        max    = temp[1]
	    indx   = temp[2]
        argmax = actionspace(s)[[1],:]

        for j = 2:size(actionspace(s))[1]
            a   = actionspace(s)[[j],:]
	        qsa = q[indx+j-1,7]

            if max < qsa
                max    = qsa
                argmax = a
            end
        end

	pol[k,5:6] = argmax
    end

    println("DONE!")
    =#

end


# We will need an arbitrary soft policy, which we predefine as "p(s)". For
# a given state s = [xpos ypos xvel yvel] it returns a random action
# a = [changexvel changeyvel] from the actionspace(s) of s.

function p(s)

    local chosena     = []
    local Actionspace = actionspace(s)
    local exploit     = rand()

    if exploit > epsi
        chosena = callPol(s)
    else
        chosena = Actionspace[ [rand(1:size(Actionspace)[1])] , : ]
    end

    return(chosena)
end





#           THIRD:
#           We define the "plotepisode" function

function plotepisode()


    # Now we are ready to create an episode using our learned policy pol. We
    # plot this episode on our racetrack:

    local E = createepisode(callPol,[],false)
    local s = E[:,1:4]
    local plotep1 = [Int(x < 0) for x in racetrack]

    for k = 1:size(s)[1]
        plotep1[s[k,1],s[k,2]] = k ;
    end

    println()
    println("The Agent started at (",s[1,1],", ",s[1,2],")" )
    println()
    return(rotl90(plotep1))

end


println()
println("SARSA_for_plot.jl is included. You may choose from 3 different functions:")
println()
println("   * createenvironment(i,v)")
println("   * train(n,gammay)")
println("   * plotepisode()")
println()
