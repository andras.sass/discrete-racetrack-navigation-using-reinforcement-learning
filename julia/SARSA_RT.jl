﻿###                                                             ###
###                                                             ###
###                  SARSA for the RACETRACK PROBLEM            ###
###                                                             ###
###                                                             ###



#   1.) compile this file via:
#           julia> include("SARSA_for_plot.jl")
#
#   2.) run the "createenvironment" function via:
#           julia> createenvironment(i,v)
#
#       i...racetrack number:
#               * i=1 small racetrack
#               * i=2 medium racetrack
#               * i=3 large racetrack
#       v...velocity options:
#               * v=1 low velocities are possible
#               * v=2 medium velocities are possible
#               * v=3 high velocities are possible
#
#
#   3.) run the "train" function via:
#           julia> train(n,gammay)
#
#       n      ... number of training episodes:
#                   * The bigger the environment the bigger the n should be.
#                   * n has a default value depending on the size of the environment.
#       gammay ... farsightedness of the Agent:
#                   * gamma should be between 0 and 1.
#                   * The bigger gamma, the more farsighted the agent will be
#                     when learning.
#
#
#   4.) plot an example episode using your calculated policy via:
#           julia> plotepisode()
#



# CODE:




#           FIRST:
#           We define the "createenvironment" function

function createenvironment(i::Int64 = 1 , v::Int64 = 1)



    # We generate the selected RACETRACK (i=1,2,3), which is a 3-dimensional
    # kxk matrix:

    #   Each entry of the matrix corresponds to one field on the racetrack.
    #   The possible entries of the matrix are -1,0,1,2 :
    #      -1...not part of the track
    #       1...part of the finish line
    #       2...part of the starting line
    #       0...part of the track, but neither finish line nor starting line

    # ATTENTION:    The Racetrack should be visualized as the matrix turned
    #               by 90° counterclockwise.
    # BECAUSE:      A point (a,b) in the standard (x,y)-coordinate system we are
    #               used to goes a to the right and b up.
    #               But when accesing a matrix at the point (a,b) we go a down
    #               and b to the left.


    if     i == 1
        global racetrack = [-1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1;
                            -1 -1 -1 -1 -1  1  1  1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];

        global rewards = [0  0  0  0  0  0  0  0  0  0;
                          0  0  0  0  0  0  0  0  0  0;
                          0  0 -1 -1 -1 -1 -1 -1  0  0;
                          0  0 -1 -1 -1 -1 -1 -1  0  0;
                          0  0 -1 -1 -1 -1 -1 -1  0  0;
                          0  0  0  0  0 -1 -1 -1  0  0;
                          0  0  0  0  0 -1 -1 -1  0  0;
                          0  0  0  0  0 -1 -1 -1  0  0;
                          0  0  0  0  0 100 100 100  0  0;
                          0  0  0  0  0  0  0  0  0  0] ;
    elseif i == 2
        global racetrack = [-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1  2  0  0  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1  0  0  0  0  0  0  1 -1;
                            -1 -1 -1 -1 -1  0  0  0  0  0  0  1 -1;
                            -1 -1 -1 -1 -1  0  0  0  0  0  0  1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
                            -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];

        global rewards =   [0  0  0  0  0  0  0  0  0  0  0  0  0;
                            0  0  0  0  0  0  0  0  0  0  0  0  0;
                            0  0 -1 -1 -1 -1 -1 -1  0  0  0  0  0;
                            0  0 -1 -1 -1 -1 -1 -1  0  0  0  0  0;
                            0  0 -1 -1 -1 -1 -1 -1  0  0  0  0  0;
                            0  0  0  0  0 -1 -1 -1  0  0  0  0  0;
                            0  0  0  0  0 -1 -1 -1  0  0  0  0  0;
                            0  0  0  0  0 -1 -1 -1  0  0  0  0  0;
                            0  0  0  0  0 -1 -1 -1  0  0  0  0  0;
                            0  0  0  0  0 -1 -1 -1  0  0  0  0  0;
                            0  0  0  0  0 -1 -1 -1 -1 -1 -1 100 0;
                            0  0  0  0  0 -1 -1 -1 -1 -1 -1 100 0;
                            0  0  0  0  0 -1 -1 -1 -1 -1 -1 100 0;
                            0  0  0  0  0  0  0  0  0  0  0  0  0;
                            0  0  0  0  0  0  0  0  0  0  0  0  0];
    elseif i == 3
        global racetrack = [];
    end



    # We set the maximum x and y coordinates which our agent can reach and the
    # maximum velocities:

    global nx = length(racetrack[:,1]) ;
    global ny = length(racetrack[1,:]) ;

    global maxv = 1 + v ;
        # This is equivalent to:
        #        if v == 1
        #            maxv = 2 ;
        #        elseif v ==2
        #            maxv = 3 ;
        #        elseif v == 3
        #            maxv = 4 ;
        #        end



    # We determine the startfields:

    global startfields = [] ;
    for x = 1:nx
        for y = 1:ny
            if racetrack[x,y] == 2
                startfields = [ startfields ; [x y] ] ;
            end
        end
    end

    global numstart  = size(startfields)[1] ;

    # We generate the STATESPACE, which is a matrix:
    #       [x,y,vv,hv;
    #        x,y,vv,hv;
    #        x,y,vv,hv;
    #            :
    #            :
    #        x,y,vv,hv]
    #
    # Every row corrsponds to one state, which the agent can take:
    #        [x,y,hv,vv] ... One possible state:
    #                 x  ... horizontal position (only positive)
    #                 y  ... vertical position  (only positive)
    #                 hv ... horizontal velocity:
    #                           * hv = -m ... Agent goes m fields to the left
    #                           * hv = m ... Agent goes m fields to the right
    #                 vv ... vertical velocity:
    #                           * vv = -m ... Agent goes m fields down
    #                           * vv = m ... Agent goes m fields up


    global S = [] ;

    for x = 1:nx
        for y = 1:ny
            if racetrack[x,y] != -1
                for hv = -maxv:maxv
                    for vv = -maxv:maxv
                            S = [ S ; [x y hv vv] ]    ;
                    end
                end
            end
        end
    end

    global ns = size(S)[1] ;


    # We create the function actionspace(s), which returns the actionspace
    #
    #           A(s) = [changehv,changevv
    #                   changehv,changevv
    #                           :
    #                           :
    #                   changehv,changevv]
    #
    # of the agent, when he is in state s = [x,y,hv,vv], where each row
    # [changehv,changevv] corresponds to one posible action the agent can take
    # when he is in state s.

    global function actionspace(s)

        local A                                     =   []       ;
        local resultingvelocity::Array{Float64,2}   =   [0 0]    ;
        local currentvelocity::Array{Float64,2}     =   s[[3 4]] ;

        for k = -1:1
            for j = -1:1

                resultingvelocity   =   currentvelocity .+ [k j]    ;
                if  (   abs(resultingvelocity[1])  <=   maxv  &&
                        abs(resultingvelocity[2])  <=   maxv  &&
                        resultingvelocity          !=   [0 0]   )

                    A = [A ; [k j]] ;
                end

            end
        end
        return A

    end

    # We want to define a function which gives back the agents next state, when
    # chooses an action a in state s. Before we can do that we need a function
    # whih tells us which fields would be visited by the agent, if he goes
    # along his decided path. We need the fieldsvisited function because we have
    # to check, whether the agent will be still on the track and if he will cross
    # the finish line or not.


    # Next we generate the function 'fieldsvisited(s,a)' , which receives a
    # a state s = [xpos,ypos,xvel,yvel] and an action a = [changehv, changevv]
    # and gives back an array of all the fields that are visited by the agent if
    # he is in state s, takes action a and takes a step. So we get:
    #
    #           fieldsvisited(s,a) = [x y
    #                                 x y
    #                                 x y
    #                                  :
    #                                  :
    #                                 x y]  ,
    #
    # where [x y] is the x and y coordinate of a field the agent visited in this
    # time step.



    global function fieldsvisited(s,a)

        local position       =   s[[1 2]]       ;
        local velocity       =   s[[3 4]] .+ a  ;
        local F              =   []                      ;
        local currentfield   =   position         ;
        local endfield       =   position .+ velocity    ;
        #vel =   [s[2][1]+a[1] s[2][2]+a[2]]     ;
        #F   =   [s[1][1]+vel[1] s[1][2]+vel[2]] ;
        #pos =   [s[1][1] s[1][2]]               ;
        #posv=   pos                             ;



        if sign(velocity[1]) == 0
            for k = 1:abs(velocity[2])
                F = [F ; [position[1] position[2]+k*sign(velocity[2])]]   ;
            end
        elseif sign(velocity[2]) == 0
            for k = 1:abs(velocity[1])
                F = [F ; [position[1]+k*sign(velocity[1]) position[2]]]   ;
            end
        elseif sign(velocity[1]) == sign(velocity[2])
            if sign(velocity[1]) == 1

                while currentfield != endfield
                    s1 =  ((currentfield[1]+1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2) ;
                    d1 = (currentfield[1]+1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2 ;

                    s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]+1-position[2])) / (velocity[1]^2+velocity[2]^2);
                    d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]+1-(position[2]+s2*velocity[2]))^2;

                    if d1 < d2
                        currentfield = currentfield .+ [1 0]  ;
                        F            = [F;currentfield]       ;
                    elseif d1 > d2
                        currentfield = currentfield .+ [0 1]  ;
                        F            = [F;currentfield]       ;
                    else
                        currentfield = currentfield .+ [1 0]  ;
                        F            = [F ; currentfield]     ;
                        currentfield = currentfield .+ [-1 1] ;
                        F            = [F ; currentfield]     ;
                        currentfield = currentfield .+ [1 0]  ;
                        F            = [F ; currentfield]     ;
                    end
                end
            elseif sign(velocity[1]) == -1

                while currentfield != endfield
                    s1 =  ((currentfield[1]-1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2) ;
                    d1 = (currentfield[1]-1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2 ;

                    s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]-1-position[2])) / (velocity[1]^2+velocity[2]^2);
                    d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]-1-(position[2]+s2*velocity[2]))^2;

                    if d1 < d2
                        currentfield = currentfield .+ [-1 0] ;
                        F            = [F;currentfield]       ;
                    elseif d1 > d2
                        currentfield = currentfield .+ [0 -1] ;
                        F            = [F;currentfield]       ;
                    else
                        currentfield = currentfield .+ [-1 0] ;
                        F            = [F;currentfield]       ;
                        currentfield = currentfield .+ [1 -1] ;
                        F            = [F;currentfield]       ;
                        currentfield = currentfield .+ [-1 0] ;
                        F            = [F ; currentfield]     ;
                    end
                end
            end
        elseif sign(velocity[1]) != sign(velocity[2])
            if sign(velocity[1]) == 1

                while currentfield != endfield
                    s1 =  ((currentfield[1]+1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2) ;
                    d1 = (currentfield[1]+1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2 ;

                    s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]-1-position[2])) / (velocity[1]^2+velocity[2]^2);
                    d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]-1-(position[2]+s2*velocity[2]))^2;

                    if d1 < d2
                        currentfield = currentfield .+ [1 0]   ;
                        F            = [F;currentfield]        ;
                    elseif d1 > d2
                        currentfield = currentfield .+ [0 -1]  ;
                        F            = [F;currentfield]        ;
                    else
                        currentfield = currentfield .+ [1 0]   ;
                        F            = [F;currentfield]        ;
                        currentfield = currentfield .+ [-1 -1] ;
                        F            = [F;currentfield]        ;
                        currentfield = currentfield .+ [1 0]   ;
                        F            = [F ; currentfield]      ;
                    end
                end

            elseif sign(velocity[1]) == -1

                while currentfield != endfield
                    s1 =  ((currentfield[1]-1)*velocity[1]+currentfield[2]*velocity[2]-position[1]*velocity[1]-position[2]*velocity[2]) / (velocity[1]^2+velocity[2]^2) ;
                    d1 = (currentfield[1]-1-(position[1]+velocity[1]*s1))^2 + (currentfield[2]-(position[2]+velocity[2]*s1))^2 ;

                    s2 = (velocity[1]*(currentfield[1]-position[1])+velocity[2]*(currentfield[2]+1-position[2])) / (velocity[1]^2+velocity[2]^2);
                    d2 = (currentfield[1]-(position[1]+s2*velocity[1]))^2 + (currentfield[2]+1-(position[2]+s2*velocity[2]))^2;

                    if d1 < d2
                        currentfield = currentfield .+ [-1 0] ;
                        F            = [F;currentfield]       ;
                    elseif d1 > d2
                        currentfield = currentfield .+ [0 1]  ;
                        F            = [F;currentfield]       ;
                    else
                        currentfield = currentfield .+ [-1 0] ;
                        F            = [F;currentfield]       ;
                        currentfield = currentfield .+ [1 1]  ;
                        F            = [F;currentfield]       ;
                        currentfield = currentfield .+ [-1 0] ;
                        F            = [F ; currentfield]     ;
                    end
                end
            end
        end
    return(F)
    end


    # So now we can construct the nextstate(s,a) function, which again receives
    # the agents current state s = [xpos, ypos, xvel, yvel] and the action
    # a = [changexv, changeyv] , which the agent is taking:
    #
    #           nextstate(s,a) = [newxpos,newypos,newxvel,newyvel,bool] ,
    #
    # where:     newxpos ... Agents new x - Position
    #            newypos ... Agents new y - Position
    #            newxvel ... Agents new x - Velocity
    #            newyvel ... Agents new y - Velocity
    #            bool    ... Finished or not?


    global function nextstate(s,a)


        local F      = fieldsvisited(s,a)     ;
        local newvel = s[[3 4]] .+ a ;
        local newpos = []                     ;
        local fs     = []                     ;
        local x      = 0                      ;
        local y      = 0                      ;

        local crossedfinish = [false,0]       ;
        local crossedborder = [false,0]       ;
        local finished      = false           ;


        for k = 1:size(F)[1]
          x = F[k,1];
          y = F[k,2];
          if   racetrack[x,y] == 1
            crossedfinish = [true, k] ;
            break
          elseif racetrack[x,y] == -1
            crossedborder = [true, k] ;
            break
          end
        end

        if ( crossedfinish[1] == true &&
             crossedborder[1] == true &&
             crossedfinish[2]  < crossedborder[2] )

          newpos   = F[ [crossedfinish[2]] , : ] ;
          finished = true                                 ;

        elseif ( crossedfinish[1] == true &&
                 crossedborder[1] == false   )

          newpos   = F[ [crossedfinish[2]] , : ] ;
          finished = true                                 ;

        elseif crossedborder[1] == true

            newpos = startfields[ [rand(1:size(startfields)[1])] , : ] ;
            newvel = [0 0] ;
        else

          newpos = s[[1 2]] .+ newvel ;

        end

        fs = [newpos  newvel finished] ;
        return (fs)
    end

    # !!!ATTENTION!!! Part of the next step is alredy part of the MC - learning
    # algorithm! We do it here because like this we save runtime when training
    # and we can call consequtive "train" functions and e dont destroy our
    # already learnt Q and C values.

    # Now we implement the Q(s,a) - Values, which we eventually want to calculate
    # to derive the optimal policy. We save the Q - Values in an array as follows:
    #
    #       q = [xpos ypos xvel yvel changexvel changeyvel Q(s,a)
    #            xpos ypos xvel yvel changexvel changeyvel Q(s,a)
    #                :
    #                :
    #            xpos ypos xvel yvel changexvel changeyvel Q(s,a)]  ,
    #
    # so the first 4 entries in each row make up the state s, the 2 following
    # define the action a and at the 7th position in each row we save the actual
    # Q - Value for the state-action pair (s,a).

    # Later, when we are using the learning algorithm, we don't want to work with
    # messy matrix-indices, so we also define 2 functions:
    #
    #   callQ(s,a)    ... returns Q - Value for the state-action pair (s,a)
    #   writeQ(s,a,v) ... saves the value v as Q(s,a)


    global q = [] ;
    for k in 1:ns
        s  = S[[k],:]       ;
        na = size(actionspace(s))[1] ;
        for j in 1:na
            a = actionspace(s)[[j],:] ;
            q = [ q ; [s a rand()] ]       ;
        end
    end

    global function callQ(s, a)

        k = find(all(q[:,1:6] .== [s a],2))[1] ;
        return((q[k,7][1],k[1]))
    end
    global function writeQ(s,a,v)

        k = find(all(q[:,1:6] .== [s a],2))[1] ;
        q[k,7] = v ;
    end


    # Next we implement our policy "pol" as a matrix, with the form:
    #
    #           pol = [xpos ypos xvel yvel changexvel changeyvel
    #                  xpos ypos xvel yvel changexvel changeyvel
    #                                   :
    #                                   :
    #                  xpos ypos xvel yvel changexvel changeyvel]  ,
    #
    # where the first 4 entries of each row represent a state s and the last 2
    # entries represent the action a which (according to the policy pol) should
    # be taken.

    # Again we will define functions for calling and writing the values of pol.

    global pol = []      ;
    for k in 1:ns
        s  = S[[k],:]           ;
        pol = [ pol ; [s 0 0] ] ;
    end
    global function writePol(s,a)

        k = find(all(pol[:,1:4] .== s ,2))[1]   ;
        pol[k,5]    =   a[1]    ;
        pol[k,6]    =   a[2]    ;
    end
    global function callPol(s)      ;
        k = find(all(pol[:,1:4] .== s ,2))[1]   ;
        a = [ pol[k,5][1]  pol[k,6][1] ] ;
        return(a)   ;
    end


    # !!!ATTENTION!!! This next step is already part of the MC-learning algorithm. We
    # do it here, because like this we save runtime and we can call the "train"
    # function repeatedly, without destroying our policy pol.

    # We initialize our policy matrix pol such that
    #
    #           pol = [s argmaxQ(s,a)
    #                  s argmaxQ(s,a)
    #                       :
    #                       :
    #                  s argmaxQ(s,a)]  .
    #
    # The Q-Values have already been initialized randomly, when the environment
    # was created.


    for k = 1:ns
        s = S[[k],:]                            ;
	    temp   = callQ(s,actionspace(s)[[1],:]) ;
        max    = temp[1] 		 	            ;
        indx   = temp[2]			            ;
        argmax = actionspace(s)[[1],:]          ;

        for j = 2:size(actionspace(s))[1]
            a   = actionspace(s)[[j],:]         ;
	        qsa = q[indx+j-1,7]			        ;

            if max < qsa
                max    = qsa      	            ;
                argmax = a                      ;
            end
        end
	    pol[k,5:6] = argmax                     ;
    end


    # Now we want define a function createepisode(p), which for a given
    # policy p = p(s) returns a matrix containing an episode of the agent in the
    # following way:
    #
    #           createepisode(p) = [ s0  a0  r1
    #                                s1  a1  r2
    #                                s2  a2  r3
    #                                     :
    #                                     :
    #                                sT [0 0] 0]  ,
    #
    # where:    si ... state in timestep i
    #                   * s0 is the initial state
    #                   * sT is the terminal state
    #           ai ... action in timestep i
    #                   * aT is always [0 0]
    #           ri ... reward in timestep i
    #                   * there is no r0
    #                   * the last reward is always 0

    global function createepisode(p::Function , stst = [] , soft = true)

        local a = []  ;
        local r = []  ;
        if stst == []
            local s = [ startfields[[rand(1:size(startfields)[1])],:] [0 0] ] ;
        else
            s = [ stst [0 0] ] ;
        end
        local Episode = []             ;
        local fs      = []             ;
        local k       = 1              ;
	    local counter	      = 1	   ;
        visited_startfields = [s[1,:]] ;

        while true
            a  =  [ a ; p(s[[k],:]) ]              ;
            k  = k+1                               ;

            fs =  nextstate(s[[k-1],:],a[[k-1],:]) ;
            r  =  [ r ; rewards[fs[1] , fs[2]] ]   ;
            s  =  [ s ; fs[[1 2 3 4]] ]            ;
            if fs[5] == true
                break
            elseif soft == false
        		if fs[[3 4]] == [0 0] && counter >= numstart
                    println("Following the policy, the agent didnt manage to get to the finish line. He started at: (",s[1,1],", ",s[1,2],")" )
                    r[end] = -1 ;
                    break
                end
                if fs[[3 4]] == [0 0] && (in(vec(fs[[1 2 3 4]]),visited_startfields)==false)

        		    visited_startfields = [ visited_startfields [ vec(fs[[1 2 3 4]]) ] ]	;
        		    counter = counter+1 ;

                end
            end
        end

        a = [a ; [0 0]]       ;
        r = [r ; 0]           ;

        Episode = [s  a  r]   ;

        return(Episode)
    end


    # finally we define a counting variable tot_ep, which counts how many training
    # episodes have been done totally.

    global tot_ep = 0           ;

    # Also we define a bollean variable, which tells us whether the training algorithm
    # has converged yet or not.

    global converged = false    ;

    # Also a variable which represents the reward, which is received when Following
    # the calculated policy.

    global previousR = -Inf     ;


    println()
    println("A new environment + agent has been created. Ready for train(n,gammay)")
    println()

end

# function createenvironment(j::Int64 = 1 , w::Int64 = 1)
#
#     global created = false ;
#
#     while created == false
#         try
#             createenv(j,w)
#             created = true
#         catch
#             created = false
#             sleep(1)
#         end
#     end
#
# end




#           SECOND:
#           We define the "train" function

function train(n::Int64 = 0 , gammay::Float64 = 0.9)

    # We will need an arbitrary soft policy, which we predefine as "p(s)". For
    # a given state s = [xpos ypos xvel yvel] it returns a random action
    # a = [changexvel changeyvel] from the actionspace(s) of s.

    global function p(s)

        local chosena = []                               ;
        local size_actionspace = size(actionspace(s))[1] ;
        local create_randomness = []                     ;

        local greedy = true                              ;
        local explore_or_exploit = rand()                ;

        if explore_or_exploit > eps
            greedy = true ;
            max = callQ(s,actionspace(s)[[1],:])[1] ;
            argmax = actionspace(s)[[1],:]          ;

            for j = 1:size(actionspace(s))[1]
                a = actionspace(s)[[j],:]           ;

                if max < callQ(s,a)[1]
                    max    = callQ(s,a)[1]          ;
                    argmax = a                      ;
                end
            end
            chosena = argmax ;

        else
            probab = rand(1:size_actionspace)    ;
            chosena = actionspace(s)[[probab],:] ;
        end

        return(chosena)
    end


    # We initialize eps and alpha, which will gradually decrease after every
    # training episode, following the formulas:
    #       eps   = 1/sqrt(t)
    #       alpha = 1/sqrt(t) ,
    # where tot_ep refers to the total number of training episodes done until
    # then.

    if tot_ep == 0
        global eps   = 0.999 ;
        global alpha = 0.999 ;
    end



    # Until now, all we did, was prepare all the tools, to be able to implement
    # the actual algorithm. Thats what we are doing now:
    #(Q is already initialized.)


    for l = 1:n

        local terminal::Bool = false                ;
        local probab = rand(1:numstart)             ;
        local s = [startfields[[probab],:]  [0 0]]  ;
        local a = p(s)                              ;


        while terminal == false

            next_s = nextstate(s,a)[[1],1:4]   ;
            r = rewards[next_s[1], next_s[2]]  ;

            next_a = p(next_s)                 ;

            writeQ(s , a , callQ(s,a)[1] + alpha * ( r + gammay * callQ(next_s,next_a)[1] - callQ(s,a)[1] ) ) ;

            s = next_s ;
            a = next_a ;

            if racetrack[s[1],s[2]] == 1
                terminal = true ;
            end

        end

        println("Episodes done: ",l ," / ",n )
        global tot_ep = tot_ep + 1 ;

        eps   = 1/(tot_ep^(1/4)) ;
        alpha = 1/(tot_ep^(1/4)) ;

    end

    println()
    println("TRAINING DONE! NUMBER OF TOTAL TRAINING-EPISODES: ",tot_ep)
    println()

    println("Now the resulting policy is being evaluated from the calculated Q-values...")
    println()

    for k = 1:ns
        s      = S[[k],:]                          ;
	    temp   = callQ(s,actionspace(s)[[1],:])    ;
        max    = temp[1] 		 	               ;
	    indx   = temp[2]			               ;
        argmax = actionspace(s)[[1],:]             ;

        for j = 2:size(actionspace(s))[1]
            a   = actionspace(s)[[j],:]            ;
	        qsa = q[indx+j-1,7]			           ;

            if max < qsa
                max    = qsa      	               ;
                argmax = a                         ;
            end
        end

	pol[k,5:6] = argmax                            ;
    end

    println("DONE!")

end





#           THIRD:
#           We define the "plotepisode" function

function plotepisode()

    # First we define our policy as a function "policy(s)", which returns the action
    # a, that our pol would use.

    global function policy(s)
        return(callPol(s))
    end


    # Now we are ready to create an episode using our learned policy pol. We
    # plot this episode on our racetrack:

    local E = createepisode(policy,[],false)        ;
    local s = E[:,1:4]  	                        ;
    local a = E[:,5:6]                              ;
    local r = E[:,7]                                ;
    local plotep1 = [Int(x < 0) for x in racetrack] ;

    for k = 1:size(s)[1]
        plotep1[s[k,1],s[k,2]] = k ;
    end

    println()
    println("The Agent started at (",s[1,1],", ",s[1,2],")" )
    println()
    return(rotl90(plotep1))

end


println()
println("SARSA_for_plot.jl is included. You may choose from 3 different functions:")
println()
println("   * createenvironment(i,v)")
println("   * train(n,gammay)")
println("   * plotepisode()")
println()
