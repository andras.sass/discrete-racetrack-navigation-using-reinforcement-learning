function gen_RT( rt_size::Int64 = 15 , path_size::Int64 = 3 , edge_size::Int64 = 5 )::Array{Int64,2}

# Path size has to be smaller than the size of the racetrack!

    if path_size >= rt_size
         error("path size is bigger then the size of the RT!")
    end

# End

# Initialize the racetrack, the first block of the racetrak and the starting line.

    local racetrack::Array{Int64,2} = fill(-1,rt_size,rt_size)
    local final_size::Int64         = 2 * edge_size + rt_size
    local starting_corner::Int64    = rand(0:3)
    local starting_edge::Int64      = rand(0:1)
    local temp::Array{Int64,2}      = rotr90(racetrack,starting_corner)

    temp[1:path_size,1:path_size] = 0

    if iseven(starting_corner)
        if iseven(starting_edge)
            temp[1:path_size,1] = 2
        else
            temp[1,1:path_size] = 2
        end
    else
        if iseven(starting_edge)
            temp[1,1:path_size] = 2
        else
            temp[1:path_size,1] = 2
        end
    end

    temp = [fill(-1,edge_size,final_size) ;
            [fill(-1,rt_size,edge_size) temp fill(-1,rt_size,edge_size)] ;
            fill(-1,edge_size,final_size)]

# End

# Create the simplified world with the first block of the track

    local simple_rt_size::Int64     = floor(rt_size/path_size)
    local simple_rt::Array{Int64,2} = [fill(-2,1,simple_rt_size+2) ;
                                       [fill(-2,simple_rt_size,1) fill(-1, simple_rt_size, simple_rt_size) fill(-2,simple_rt_size,1)] ;
                                       fill(-2,1,simple_rt_size+2)]
    simple_rt[2,2] = 0

# End

# Create Array which contains the visited and current position.
    local positions::Array{Int64,2} = [2 2]
# End

# Create a track in the simplified world

    while true
        local current_position::Array{Int64,2} = positions[[end],:]
        local poss_pos::Array{Int64}         = possible_positions(current_position , simple_rt)
        #println("possible positions from the current position ", current_position, " , are ", poss_pos)
        if isempty(poss_pos)
            break
        end
        pick = rand(1:size(poss_pos)[1])
        simple_rt[poss_pos[pick,1],poss_pos[pick,2]] = 0
        positions = [positions ; poss_pos[[pick],:]]

        if simple_rt[current_position[1]+1,current_position[2]] == -1
            simple_rt[current_position[1]+1,current_position[2]] = -2
        end
        if simple_rt[current_position[1]-1,current_position[2]] == -1
            simple_rt[current_position[1]-1,current_position[2]] = -2
        end
        if simple_rt[current_position[1],current_position[2]+1] == -1
            simple_rt[current_position[1],current_position[2]+1] = -2
        end
        if simple_rt[current_position[1],current_position[2]-1] == -1
            simple_rt[current_position[1],current_position[2]-1] = -2
        end
    end

# End
# Expand the simplified track onto the real one.

    positions = positions[2:end,:] .-1
    track_parts = [positions[i,:] for i in 1:size(positions)[1]]
    for i in track_parts
        zw11 = (i[1]-1)*path_size+1+edge_size
        zw12 = i[1]*path_size+edge_size
        zw21 = (i[2]-1)*path_size+1+edge_size
        zw22 = i[2]*path_size+edge_size
        temp[zw11:zw12,zw21:zw22] = 0
    end

# End
# Create the finish line

    pos_fin::Int64 = rand(1:4)
    if pos_fin == 1
        temp[(positions[end,1]-1)*path_size+1+edge_size,(positions[end,2]-1)*path_size+1+edge_size:(positions[end,2]-1)*path_size+1+edge_size+path_size-1] = 1
    end
    if pos_fin == 2
        temp[(positions[end,1]-1)*path_size+1+edge_size+path_size-1,(positions[end,2]-1)*path_size+1+edge_size:(positions[end,2]-1)*path_size+1+edge_size+path_size-1] = 1
    end
    if pos_fin == 3
        temp[(positions[end,1]-1)*path_size+1+edge_size:(positions[end,1]-1)*path_size+1+edge_size+path_size-1,(positions[end,2]-1)*path_size+1+edge_size] = 1
    end
    if pos_fin == 4
        temp[(positions[end,1]-1)*path_size+1+edge_size:(positions[end,1]-1)*path_size+1+edge_size+path_size-1,(positions[end,2]-1)*path_size+1+edge_size+path_size-1] = 1
    end
# End

    racetrack = rotl90(temp,starting_corner)
end



function possible_positions(current_position::Array{Int64,2} , simple_rt::Array{Int64,2})

    local pospos = []

    if simple_rt[current_position[1]+1,current_position[2]] == -1
        pospos = [pospos ; [current_position[1]+1 current_position[2]] ]
    end
    if simple_rt[current_position[1]-1,current_position[2]] == -1
        pospos = [pospos ; [current_position[1]-1 current_position[2]] ]
    end
    if simple_rt[current_position[1],current_position[2]+1] == -1
        pospos = [pospos ; [current_position[1] current_position[2]+1] ]
    end
    if simple_rt[current_position[1],current_position[2]-1] == -1
        pospos = [pospos ; [current_position[1] current_position[2]-1] ]
    end
    return pospos
end
