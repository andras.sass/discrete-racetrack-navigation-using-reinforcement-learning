# Calculate the average rewards on 300 episodes for MC and SARSA over 100 trials.

# 300,30,1 ... 30 * 1 + 30 * 5.5 = ~ 3.5 Stunden
num_eps    = 300
num_trials = 30
env_size   = 2


x = collect(1:num_eps)
y = zeros(num_eps,2)


# FIRST WE CALCULATE THE AVERAGE RETURNS WITH MONTE CARLO:

	include("MC_for_plot.jl")

	for trial = 1:num_trials

		#calculate rewards "rews" with MC

		rews = zeros(num_eps)

		println()
		println()
		println()
		println("-------------  Environment number ", trial, " / ", num_trials, " is being created...(MC)  ---------------")

		createenvironment(env_size,1)

		println()
		println("---------------------------------  created.  -----------------------------------")


		global function policy(s)
	        return(callPol(s))
	    end

		for episo = 1:num_eps
			train(1)
			for count = 1:numstart
				r = sum(createepisode(policy,startfields[count,:]',false)[:,7]) ;
				rews[episo] += r/numstart
			end
			println("In trial number ", trial, " / ", num_trials, " in the ", episo, " / ", num_eps, " episode, the average reward is " , rews[episo])
		end

	y[:,1] = y[:,1] + rews

	end

	y[:,1] = y[:,1]/num_trials

	using JLD
	@save "rewards_medium_MC.jld"




# NOW WE CALCULATE THE AVERAGE REWARDS WITH SARSA:


  	include("SARSA_for_plot.jl")


  	for trial = 1:num_trials

  		#calculate rewards "rews" with SARSA

  		rews = zeros(num_eps)

  		println()
  		println()
  		println()
  		println("-----------  Environment number ", trial, " / ", num_trials, " is being created...(SARSA)  --------------")

  		createenvironment(env_size,1)

  		println()
  		println("---------------------------------  created.  -----------------------------------")


  		global function policy(s)
  	        return(callPol(s))
  	    end

  		for episo = 1:num_eps
  			train(1)
  			for count = 1:numstart
  				r = sum(createepisode(policy,startfields[count,:]',false)[:,7]) ;
  				rews[episo] += r/numstart
  			end
  			println("In trial number ", trial, " / ", num_trials, " in the ", episo, " / ", num_eps, " episode, the average reward is " , rews[episo])
  		end

  	y[:,2] = y[:,2] + rews

  	end

  	y[:,2] = y[:,2]/num_trials


 using JLD
 @save "rewards_medium_SARSA.jld"

# # #	plot(x,y[:,1])
# #
# # #	plot(x,y[:,2])
