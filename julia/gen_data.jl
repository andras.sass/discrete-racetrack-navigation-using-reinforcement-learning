# Generate Data which can be used to train a NN. We ganerate racetracks and
# determine th eoptimal policies which to follow on these specific ractracks.
# The data will be safed in hdf5 format, so it can be used later on in python.
using HDF5
include("gen_rand_RT.jl")
include("Value_It_for_data.jl")


function generate_data(num_RT::Int64 = 10, size_RT::Int64 = 15, size_path::Int64 = 3, acc_Pol::Float64 = 25.0)

    for i = 1:num_RT

        println()
        info("------- Generating random racetrack number $(i) / $(num_RT) -------")

        local rt = gen_RT(size_RT,size_path,5)

        info("                             Done!")
        println()
        info("Creating environment number $(i) / $(num_RT) ...")

        createenvironment(rt,1)

        info("Done!")
        println()
        info("Now training is going on ...")

        train() #If you want a more accurate Policy, the in Value_It decrease
                # delta

        info("Done!")
        println()
        info("Saving the calculated Data of racetrack number $(i) / $(num_RT) ...")

        local name_rt  = "racetrack/$(i)"
        local name_pol = "policy/$(i)"

        h5write("rt_pol.h5", name_rt,  rt )
        h5write("rt_pol.h5", name_pol, pol)

        info("Done!")
        println()
        println()

    end

end
